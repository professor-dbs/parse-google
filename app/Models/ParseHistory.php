<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParseHistory extends Model
{
    protected $fillable = [
        'search', 'domain', 'position'
    ];
}
