<?php

namespace App\Http\Controllers;

use App\Models\ParseHistory;
use Illuminate\Http\Request;
use \Symfony\Component\DomCrawler\Crawler;


class ParseController extends Controller
{
    public function custom_curl($url)
    {
        $proxys = [
            '144.217.74.219:3128',
            '54.39.53.104:3128',
            '149.56.133.81:3128',
            '167.71.94.127:3128',
            '167.71.105.166:3128',
            '167.71.182.191:3128',
            '167.114.197.123:3128',
            '68.183.196.82:8080',
            '178.128.225.180:8080',
            '167.71.97.146:3128',
            '167.71.186.105:3128',
            '167.71.105.170:3128',
            '167.71.106.246:3128',
            '167.71.103.168:3128',
            '140.82.42.243:8080',
            '167.71.252.107:3128',
            '198.98.51.240:8080',
            '167.71.254.86:3128',
            '167.71.97.177:3128',
            '167.71.182.13:3128',
            '51.79.29.176:8080',
            '167.71.182.175:3128',
            '167.71.186.103:3128',
            '198.98.55.168:8080',
            '102.165.53.62:8080',
        ];
        $proxy = $proxys[array_rand($proxys)];
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);
        curl_setopt($curl, CURLOPT_PROXY, $proxy);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);
        $output = rtrim($response);
        $data = explode("\r\n", $output);
        $status = $data[0];

        if ($err) {
            return ['status' => $status, 'response' => $err];
        } else {
            return ['status' => $status, 'response' => $response];
        }
    }

    public function index()
    {
        return view('index');
    }

    public function parse(Request $request)
    {
        do {
            $html = $this->custom_curl('http://www.google.com/search?q=' . $request->search . '&num=100');
        } while ($html['status'] != 'HTTP/1.1 200 OK');

        $crawler = new Crawler((string)$html['response']);
        $sites = $crawler->filter('.BNeawe.UPmit.AP7Wnd')->extract(['_text']);
        $position = "Not found";
        foreach ($sites as $key => $site) {
            if (trim(explode('›', $site)[0]) == $request->domain) {
                $position = $key + 1;
                break;
            }
        }
        ParseHistory::create([
            'search' => $request->search,
            'domain' => $request->domain,
            'position' => $position
        ]);
        return redirect()->route('index')->with('result', 'Домен "' . $request->domain . '" по ключевому слову "' . $request->search . '" находится на ' . $position . ' позиции');
    }
}
