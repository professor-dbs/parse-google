<?php

namespace App\Http\Controllers;

use App\Models\parseHistory;
use Illuminate\Http\Request;

class ParseHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('history', [
                'histories' => ParseHistory::all()
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\parseHistory $parseHistory
     * @return \Illuminate\Http\Response
     */
    public function show(parseHistory $parseHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\parseHistory $parseHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(parseHistory $parseHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\parseHistory $parseHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, parseHistory $parseHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\parseHistory $parseHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(parseHistory $parseHistory)
    {
        //
    }
}
