@extends('layouts.app')

@section('content')
    <table data-toggle="table" data-sort-name="id" data-sort-order="desc">
        <thead>
        <tr>
            <th data-field="id" data-sortable="true"> ID </th>
            <th data-field="domain" data-sortable="true"> Доменное имя </th>
            <th data-field="keyword" data-sortable="true"> Ключевое слово </th>
            <th data-field="date" data-sortable="true"> Дата выборки </th>
        </tr>
        </thead>
        <tbody>
        @foreach($histories as $history )
            <tr>
                <td>{{$history->id}}</td>
                <td>{{$history->domain}}</td>
                <td>{{$history->search}}</td>
                <td>{{$history->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.15.4/dist/bootstrap-table.min.css">
@endsection
