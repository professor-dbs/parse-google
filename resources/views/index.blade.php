@extends('layouts.app')

@section('content')
    @if (\Session::has('result'))
        <div class="alert alert-success">
            {{Session::get('result')}}
        </div>
    @endif
    <form action="{{ route('parse') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="domain">Введите домен</label>
            <input type="text" class="form-control" id="domain"  name="domain" placeholder="Введите домен">
        </div>
        <div class="form-group">
            <label for="search">Ключевое слово</label>
            <input type="text" class="form-control" id="search" name="search" placeholder="Ключевое слово">
        </div>
        <button type="submit" class="btn btn-primary">Запросить</button>
    </form>
@endsection
